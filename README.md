# codeberg-pages

Publish the builded files to the `pages` branch for deployment website on Codeberg Pages

## Started

```sh
npm install -g codeberg-pages
```
or

```sh
sudo npm install -g codeberg-pages
```

## Usage

The `codeberg-pages` command takes one parameter - the name of the directory with build files

example:

```sh
codeberg-pages dist
```

# WARN:

This package is at a very early stage of development.

Check out the website deployment [documentation on Codeberg Pages](https://docs.codeberg.org/codeberg-pages/) before use.
